package homeworkweek06.domain.calculator;

public class Calculator {

    private double km;
    private double m;
    private double dm;
    private double cm;
    private double mm;
    private double km_to_mm;
    private double m_to_mm;
    private double dm_to_mm;
    private double cm_to_mm;

    public double km_to_mm(double km) {
        this.km = km;
        System.out.println( "Input km : "+km );
        return km_to_mm = km * 1000000;
    }

    public double m_to_mm(double m) {
        this.m = m;
        System.out.println( "Input  m : " + m);
        return m_to_mm = m * 1000;
    }

    public double dm_to_mm(double dm) {
        this.dm =dm;
        System.out.println( "Input dm : " +dm );
        return dm_to_mm = dm * 100;
    }

    public double cm_to_mm(double cm) {
        this.cm = cm;
        System.out.println( "Input cm : " +cm );
        return cm_to_mm = cm * 10;
    }

    public double mm(double mm) {
        this.mm = mm;
        System.out.println( "Input mm : " + mm);
        return mm ;
    }

    public void expression() {
        System.out.println("\n" + km + "km +" + m + "m +" + dm + "dm +" + cm + "cm +" + mm + "mm =" );
    }

    public double result() {
        System.out.println( "Result is : " + (km_to_mm + m_to_mm + dm_to_mm + cm_to_mm + mm) + " mm" );
        return km_to_mm + m_to_mm + dm_to_mm + cm_to_mm + mm;

    }

//    public double getKm() {
//        return km;
//    }
//
//    public double getM() {
//        return m;
//    }
//
//    public double getDm() {
//        return dm;
//    }
//
//    public double getCm() {
//        return cm;
//    }
//
//    public double getMm() {
//        return mm;
//    }
}
