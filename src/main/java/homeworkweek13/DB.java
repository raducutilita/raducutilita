package homeworkweek13;

import java.sql.*;

public class DB {
    public static void main(String[] args) {

        Connection connection = null;
        try {
            connection = DriverManager.getConnection( "jdbc:postgresql://localhost:5432/bookingaplication", "postgres", "radu" );

        } catch (SQLException e) {
            System.err.println( "Can not connect to DB" + e.getMessage() );
        }

        if (connection == null) {
            return;
        }

        Statement statement = null;
        ResultSet resultSet = null;
//        final String format = "%5s%20s%20s\n";

        try {
            statement = connection.createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
            final String format1 = "%5s%30s%30s%30s%50s\n";
            resultSet = statement.executeQuery("select * from accommodation");

            boolean hasResults = resultSet.next();
            if (hasResults) {
                System.out.format(format1,"Id","Type" ,"Bed_type"," Max_guests","Description");
                do {
                    System.out.format( format1,resultSet.getString( "id" ) ,
                            resultSet.getString( "type" ),
                            resultSet.getString( "bed_type" ),
                            resultSet.getString( "max_guests" ),
                            resultSet.getString( "description" ));
                }while (resultSet.next());
            }else {
                System.out.println("No results");
            }

//            PreparedStatement preparedStatement1 = null;
//            try {
//                preparedStatement1 = connection.prepareStatement(
//                        "insert into accommodation values (?,?,?,?,?)");
//                preparedStatement1.setInt(1,3);
//                preparedStatement1.setString(2,"Apartment" );
//                preparedStatement1.setString(3,"1 Queen bed and 1 couch" );
//                preparedStatement1.setInt(4,3 );
//                preparedStatement1.setString(5,"1 Bedroom and 1 Living room");
//                preparedStatement1.executeUpdate();
//            } catch (SQLException e) {
//                System.err.println("Can not insert into accommodation" + e.getMessage());
//            }

            System.out.println();
            final String format2 = "%5s%30s%30s\n";
            resultSet = statement.executeQuery("select * from room_fair");

            boolean fairResults = resultSet.next();
            if (fairResults) {
                System.out.format(format2,"Id","Value" ,"Season");
                do {
                    System.out.format( format2,resultSet.getString( "id" ) ,
                            resultSet.getString( "value" ),
                            resultSet.getString( "season" ));
                }while (resultSet.next());
            }else {
                System.out.println("No results");
            }

            System.out.println();
//            PreparedStatement preparedStatement2 = null;
//            try {
//                preparedStatement2 = connection.prepareStatement(
//                        "insert into room_fair values (?,?,?)");
//                preparedStatement2.setInt(1,4 );
//                preparedStatement2.setInt(2,1000 );
//                preparedStatement2.setString(3,"Always" );
//                preparedStatement2.executeUpdate();
//            } catch (SQLException e) {
//                System.err.println("Can not insert into room_fair" + e.getMessage());
//            }

//            final String format3 = "%5s%30s%30s\n";
//            resultSet = statement.executeQuery("select * from accommodation_room_fair_relation");
//
//            boolean relationResults = resultSet.next();
//            if (relationResults) {
//                System.out.format(format3,"Id","Accommodation id","Room fair id","Foreign key accommodation" ,"Foreign key fair");
//                do {
//                    System.out.format( format3,resultSet.getString( "id" ) ,
//                            resultSet.getString( "accommodation_id" ),
//                            resultSet.getString( "room_fair_id" ));
//                }while (resultSet.next());
//            }else {
//                System.out.println("No results");
//            }

            System.out.println();
//            PreparedStatement preparedStatement3 = null;
//            try {
//                preparedStatement3 = connection.prepareStatement(
//                        "insert into accommodation_room_fair_relation values (?,?,?)");
//                preparedStatement3.setInt(1,3 );
//                preparedStatement3.setInt(2,3);
//                preparedStatement3.setInt(3,4 );
//                preparedStatement3.executeUpdate();
//            } catch (SQLException e) {
//                System.err.println("Can not insert into room_fair" + e.getMessage());
//            }

            resultSet = statement.executeQuery(
                    "SELECT *FROM "
                            + "accommodation"
                            + " NATURAL JOIN "
                            + "room_fair");

            final String format4 = "%5s%20s%20s%20s%30s%30s\n";
            // Step 5: Execute the query
            System.out.format(format4,"Type","Bed type  ","Max guests","Description","Value","Season");

            // Step 6: Process the statements
            // Iterate the resultset and retrieve the
            // required fields
            while (resultSet.next()) {
                String type = resultSet.getString("type");
                String bed_type = resultSet.getString("bed_type");
                String max_guests
                        = resultSet.getString("max_guests");
                String description = resultSet.getString("description");
                String value = resultSet.getString( "value" );
                String season = resultSet.getString( "season" );

                System.out.format(format4,
                        type,  bed_type, max_guests, description, value, season);
            }





        } catch (SQLException e) {
            System.err.println("Can not run query" + e.getMessage());
        }finally {
            if (resultSet != null) try { resultSet.close(); } catch (SQLException e) { }
            if (statement != null) try { statement .close(); } catch (SQLException e) { }
            try { connection.close(); } catch (SQLException e) { } // optional. We might reuse the same connection
        }

    }
}