package homeworkweek11;
import static homeworkweek11.Main.gate;

import java.util.List;
import java.util.Random;

public class MyThread extends Thread {

    enum TicketType {
        fullTickets, fullVIPPasses, freePasses, oneDayPasses, oneDayVIPPasses
    }

        @Override
        public void run() {

            final List<MyThread.TicketType> VALUES = List.of( MyThread.TicketType.values() );
            final int SIZE = VALUES.size();
            final Random RANDOM = new Random();

            MyThread.TicketType ticketType = VALUES.get( RANDOM.nextInt( SIZE ) );

            try {
                Thread.sleep( 100 );
                gate.addTicket( ticketType );
            } catch (InterruptedException e) {
                System.out.println( "Exception in sleep" );
            }


        }
    }

