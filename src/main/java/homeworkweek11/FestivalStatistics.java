package homeworkweek11;

import static homeworkweek11.Main.gate;

public class FestivalStatistics extends Thread {

    @Override
    public void run() {
        int group = 0;
        do {
            if (group == 0) {
                System.out.println( "Opening gate!" + "\n" );
            } else {

                System.out.println( "After " + group + " group of people: " );
                System.out.println( gate.getTotalNumberOfTickets() + " people entered" );
                System.out.println( gate.getNumberFullTickets() + " people have full tickets" );
                System.out.println( gate.getNumberFreePassTickets() + " have free passes" );
                System.out.println( gate.getNumberFullVipTickets() + " full vip passes" );
                System.out.println( gate.getNumberOneDayTickets() + " have one-day passes" );
                System.out.println( gate.getNumberOneDayVipTickets() + " have one-day vip passes" );
                System.out.println();
            }

            try {
                Thread.sleep( 5 * 1000 );

            } catch (InterruptedException e) {
                System.err.println( "Exception in sleep" );
            }

            group++;
        }
        while (group < 5);

    }
}
