package homeworkweek08;

import java.util.Comparator;

public class TimeComparator implements Comparator<Athlete> {

    @Override
    public int compare(Athlete o1, Athlete o2) {
        return (int) (o1.finalTime() - o2.finalTime());
    }




}
