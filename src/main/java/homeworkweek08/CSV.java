package homeworkweek08;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class CSV {
    public static void main(String[] args) throws IOException {

        BufferedReader bufferedReader = null;


        try {
            bufferedReader = new BufferedReader( new FileReader( "src/main/resources/athletes.csv" ) );
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println( line );
            }
        } catch (IOException e) {
            System.err.println( "Error" + e.getMessage() );
        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        }
        System.out.println("");

        Athlete athlete1 = new Athlete( 11, "Umar Jorgson", CountryCode.SLOVAKIA, 30.27, "xxxox", "xxxxx", "xxoxo" );
        Athlete athlete2 = new Athlete( 1, "Jimmy Smiles", CountryCode.UNITEDKINGDOM, 29.05, "xxoox", "xooxx", "xxxxo" );
        Athlete athlete3 = new Athlete( 27, "Piotr Smitzer", CountryCode.CZEHIA, 41.01, "xxxxx", "xxxxx", "xxxxx" );

        athlete1.finalTime();
        athlete2.finalTime();
        athlete3.finalTime();

        Set<Athlete> athleteSet = new TreeSet<>( new TimeComparator() );
        athleteSet.add( athlete1 );
        athleteSet.add( athlete2 );
        athleteSet.add( athlete3 );

        System.out.println("");

        System.out.println( "Winner - " + athlete2.getAthleteName() + " " + athlete2.finalTime() +" ("+ (athlete2.getSkyTimeResult()+ " + " + athlete2.misses())+" seconds)" );
        System.out.println( "Runner-up - " + athlete1.getAthleteName() + " " + athlete1.finalTime() +" ("+ (athlete1.getSkyTimeResult()+ " + " + athlete1.misses())+" seconds)" );
        System.out.println( "Third Place - " + athlete3.getAthleteName() + " " + athlete3.finalTime() +" ("+ (athlete3.getSkyTimeResult()+ " + " + athlete3.misses())+" seconds)" );

    }
}
