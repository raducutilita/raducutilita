package homeworkweek08;

public enum CountryCode {

    SLOVAKIA("SK"),
    UNITEDKINGDOM("UK"),
    CZEHIA("CZ");

    private String countryCode;

    CountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public String toString() {
        return countryCode ;

    }
}
