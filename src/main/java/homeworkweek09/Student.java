package homeworkweek09;


import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Student {

    static class InvalidNameException extends Exception {


        public InvalidNameException(String message) {
            super( message );

        }
    }

    enum Gender {
        M, F, X, m ,f
    }

    private String ID;
    private Gender gender;
    private String firstName;
    private String lastName;
    private int birthDate;


    public Student(String ID, Gender gender, String firstName, String lastName, int birthDate) throws InvalidNameException {

        this.ID = ID;
        this.gender = Gender.valueOf( String.valueOf( gender ) );
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate =  birthDate;


    }


    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(int birthDate) {
        this.birthDate = birthDate;
    }



    @Override
    public String toString() {
        return "Student{" +
                "ID='" + ID + '\'' +
                ", gender=" + gender +
                ", FirstName='" + firstName + '\'' +
                ", LastName='" + lastName + '\'' +
                ", BirthDate='" + birthDate + '\'' +
                '}';
    }

    public boolean isStudentValid;

    //List<Student> students = new ArrayList<>();

    public boolean validateStudents(String ID, Student.Gender gender, String firstName, String lastName, int birthDate) throws InvalidNameException {


        //Student student = new Student(ID, gender, firstName, lastName, BirthDate); //creating student object to be able to operate on it

        try {
            if ((getFirstName() == null || getFirstName().isEmpty()) || (getLastName() == null || getLastName().isEmpty())) {


                throw new InvalidNameException("first or last name missing");

            } else if (birthDate < 1900 || birthDate > 2004) {


                throw new InvalidNameException("student has age out of acceptable bounds");

            } else if ((gender.toString().equals("F"))==false || (gender.toString().equals("M"))==false) {


                throw new InvalidNameException("Gender not recognized");
            }

        } catch (InvalidNameException e) {
            System.err.println("Error while trying to validate student");
            System.out.println("The following student could not be added: " + this.toString());
            return isStudentValid = false;
        }

        return isStudentValid = true;
    }
}
