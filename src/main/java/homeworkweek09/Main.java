package homeworkweek09;

public class Main {

    public static void main(String[] args) throws Student.InvalidNameException {


        Student student1 = new Student("123456", Student.Gender.F, "Danielle", "King", 1950);
        Student student2 = new Student("140355", Student.Gender.m, "Eric", "Miller", 1999);
        Student student3 = new Student("178892", Student.Gender.F, "Mary", "", 2004);
        Student student4 = new Student("112901", Student.Gender.f, "Amanda", "Thomson", 2005);
        Student student5 = new Student("161093", Student.Gender.F, "", "Young", 1995);
        Student student6 = new Student("193665", Student.Gender.M, "Darelle", "Young", 1990);
        Student student7 = new Student("", Student.Gender.F, "Nicole", "Benson", 1889);
        Student student8 = new Student("161091", Student.Gender.M, "Anthony", "Sims", 1932);


        Repo repo = new Repo();


        repo.addStudents("123456", Student.Gender.F, "Danielle", "King", 1950);
        repo.addStudents("140355", Student.Gender.m, "Eric", "Miller", 1999);
        repo.addStudents("178892", Student.Gender.F, "Mary", "", 2004);
        repo.addStudents("112901", Student.Gender.f, "Amanda", "Thomson", 2005);
        repo.addStudents("161093", Student.Gender.F, "", "Young", 1995);
        repo.addStudents("193665", Student.Gender.M, "Darelle", "Young", 1990);
        repo.addStudents("", Student.Gender.F, "Nicole", "Benson", 1889);
        repo.addStudents("161091", Student.Gender.M, "Anthony", "Sims", 1932);

        System.out.println(repo);
        repo.removeStudents("123456");
        repo.removeStudents("1234567");
        repo.retrieveStudents(27, 1995 );
        repo.retrieveStudents(23, 1999 );

        System.out.println(repo);
        repo.addStudentToTreeSetRepo(student3);
        repo.addStudentToTreeSetRepo(student5);
        repo.addStudentToTreeSetRepo(student2);
        repo.addStudentToTreeSetRepo(student7);
        repo.addStudentToTreeSetRepo(student8);

        repo.removeStudents( "161091" );
        System.out.println(repo);
    }

}