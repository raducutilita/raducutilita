package homeworkweek04;

import homeworkweek04.domain.car.Car;
import homeworkweek04.domain.car.Vechicle;
import homeworkweek04.domain.car.dacia.DaciaDuster;
import homeworkweek04.domain.car.dacia.DaciaLogan;
import homeworkweek04.domain.car.mazda.Mazda6;
import homeworkweek04.domain.car.mazda.MazdaRX8;

public class Main {
    public static void main(String[] args) {
        Car car1 =new DaciaDuster( 60,"S1526GSDGADJF" );
        Car car2 =new DaciaLogan( 50,"DG31551MMDDSF" );
        Vechicle vehicle= new Mazda6(50,"221GKGSJAFMFS");
        Vechicle vechicle2= new MazdaRX8(50,"GSOGJS321FAFA");
        Car car3 = (Car) vehicle;
        Car car4 = (Car) vechicle2;

        car1.setTireSize( 15 );
        car1.start();
        car1.start();
        car1.shiftGear( 1 );
        car1.drive(100);
        car1.getAvailableFuel();
        car1.shiftGear( 2 );
        car1.drive(100);
        car1.shiftGear( 3 );
        car1.drive(100);
        car1.shiftGear( 5 );
        car1.drive(100);
        car1.getAvailableFuel();
        car1.stop();
        car1.getAverageFuelConsumption();

        System.out.println("");

        car2.setTireSize( 17 );
        car2.start();
        car2.shiftGear( 1 );
        car2.drive(100);
        car2.getAvailableFuel();
        car2.shiftGear( 2 );
        car2.drive(100);
        car2.shiftGear( 3 );
        car2.drive(100);
        car2.shiftGear( 5 );
        car2.drive(100);
        car2.getAvailableFuel();
        car2.stop();
        car2.getAverageFuelConsumption();

        car4.setTireSize( 20 );
        car4.start();
        car4.shiftGear( 1 );
        car4.drive(100);
        car4.getAvailableFuel();
        car4.shiftGear( 2 );
        car4.drive(100);
        car4.shiftGear( 3 );
        car4.drive(100);
        car4.shiftGear( 5 );
        car4.drive(100);
        car4.getAvailableFuel();
        car4.stop();
        car4.getAverageFuelConsumption();














    }
}
