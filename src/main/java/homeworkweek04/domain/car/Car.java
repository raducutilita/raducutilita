package homeworkweek04.domain.car;

public abstract class Car implements Vechicle {

    private final float fuelTankSize = 60f;
    private final String fuelType = "Petrol";
    private final float gears = 5;
    private final float consumptionPer100KM = 6;//consumption after 100km
    protected float availableFuel;
    protected int tireSize;
    private String chassisNumber;
    private float drive= 0.0f;//distance driven
    protected float consumptionAfterDrive;//consumption after driving in liters
    protected float increaseValue=0.1f;
    protected float increaseFuelConsumption = consumptionPer100KM;//gear up increase fuel consumption,gear down decrease fuel consumption
    private float averageFuelConsumption;//average fuel consumption (liters/hundreds of km driven)
    protected float increaseConsumption;//increase consumption tires are bigger than 15
    private String engineState;//OFF or ON


    public Car() {
    }

    public Car(float availableFuel, String chassisNumber) {
        this.availableFuel = availableFuel;
        this.chassisNumber = chassisNumber;
        setEngineState( "OFF" );
    }

    public void setTireSize(int tireSize) {
        if (tireSize==15){
            increaseConsumption = 1;
            System.out.println("Tire size 15.");
        }else if (tireSize==16){
            increaseConsumption = 1.1f;
        }else if (tireSize==17){
            increaseConsumption = 1.2f;
        }else if (tireSize==20){
            increaseConsumption = 2;
        }else{
                increaseConsumption = 0;
                System.out.println("You have no tires!");
            }
        }




    public void start(){
        if (this.engineState == "ON") {
            System.out.println( "Engine is already ON!" );
        } else {
            this.setEngineState( "ON" );
            System.out.println( "Engine is ON." );
            System.out.println("Consumption is : "+"0.0");
        }
    }


    public void shiftGear(int gears){
        if (gears==1){
            increaseFuelConsumption=consumptionPer100KM*increaseConsumption;
        }else if (gears==2){
            increaseFuelConsumption=((increaseFuelConsumption*increaseValue)+consumptionPer100KM)*increaseConsumption;
        }else if (gears==3){
            increaseFuelConsumption=((consumptionPer100KM*increaseValue)+consumptionPer100KM)*increaseConsumption;
        }else if (gears==4){
            increaseFuelConsumption=((consumptionPer100KM*increaseValue)+consumptionPer100KM)*increaseConsumption;
        }else if (gears==5){
            increaseFuelConsumption=((consumptionPer100KM*increaseValue)+consumptionPer100KM)*increaseConsumption;
        }else{
            System.out.println("Not in gear!");
        }
    }

    @Override
    public void drive(float distance) {
        drive=drive+distance;
        System.out.println("Distance driven "+ drive + " KMs.");
        consumptionAfterDrive=drive*increaseFuelConsumption/100;
        System.out.println("Consumption after distance driven " + (consumptionAfterDrive) + " liters.");
    }




    public void stop() {
        if (this.engineState == "OFF") {
            System.out.println( "Engine is already OFF!" );
        } else {
            this.setEngineState( "OFF" );
            System.out.println( "Engine is OFF." );

        }
        averageFuelConsumption=consumptionAfterDrive/(drive/100);
        System.out.println("Average fuel consumption is: "+averageFuelConsumption + " liters.");
    }

    public void getAvailableFuel() {
        if (availableFuel==0){
            System.out.println("You need to refuel!");
        }else{
            availableFuel=fuelTankSize-consumptionAfterDrive;
            System.out.println("Available fuel in tank "+availableFuel+" liters.");
        }
    }

    public float getAverageFuelConsumption() {
        return averageFuelConsumption;
    }



    public void setAvailableFuel(float availableFuel) {
        this.availableFuel = availableFuel;
    }

    public int getTireSize() {
        return tireSize;
    }

    public String getEngineState() {
        return engineState;
    }

    public void setEngineState(String engineState) {
        this.engineState = engineState;
    }

}





