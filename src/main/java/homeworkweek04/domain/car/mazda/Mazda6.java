package homeworkweek04.domain.car.mazda;

public class Mazda6 extends Mazda {

    public Mazda6() {
        System.out.println("A new Mazda 6 has been instantiated.");
    }

    public Mazda6(float availableFuel, String chassisNumber) {
        super( availableFuel, chassisNumber );
    }
}
