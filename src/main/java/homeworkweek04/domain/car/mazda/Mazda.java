package homeworkweek04.domain.car.mazda;

import homeworkweek04.domain.car.Car;


public abstract class Mazda extends Car {

    public Mazda(){

    }

    public Mazda(float availableFuel, String chassisNumber) {
        super( availableFuel, chassisNumber );
        setEngineState( "OFF" );
    }
}
