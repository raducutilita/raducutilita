package homeworkweek04.domain.car.dacia;

public class DaciaDuster extends Dacia {

    public DaciaDuster() {
        System.out.println( "A new Dacia Duster has been instantiated." );
    }

    public DaciaDuster(float availableFuel, String chassisNumber) {
        super( availableFuel, chassisNumber );

    }




    @Override
    public void drive(float distance) {
        super.drive( distance );
    }
}
