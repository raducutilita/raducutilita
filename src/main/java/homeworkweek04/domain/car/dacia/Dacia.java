package homeworkweek04.domain.car.dacia;

import homeworkweek04.domain.car.Car;

public abstract class Dacia extends Car {

    public Dacia(){

    }

    public Dacia(float availableFuel, String chassisNumber) {
        super( availableFuel, chassisNumber );
        setEngineState( "OFF" );
    }
}




