package homeworkweek05;

import java.util.HashMap;
import java.util.Map;

import homeworkweek05.domain.*;
import homeworkweek05.domain.comparator.MyAgeComparator;
import homeworkweek05.domain.comparator.MyNameComparator;


import java.util.*;

public class Main {
    public static void main(String[] args) {

        //Creating persons
        Person person1 = new Student("Gabi",20);
        Person person2 = new Hired("Stefan",50);
        Person person3 = new Unemployed("Maria",18);

        // Sorted tree-set by name
        System.out.println("Sorted by name:");
        Set<Person> personSet = new TreeSet<>(new MyNameComparator() );

        personSet.add( person1 );
        personSet.add( person2 );
        personSet.add( person3 );

        for (Person person : personSet){
            System.out.println("Name: "+ person.getName()+"  "+"Age: "+person.getAge());
        }
        System.out.println("  ");

        // Sorted tree-set by age
        System.out.println("Sorted by age:");
        Set<Person> personSet1 = new TreeSet<>(new MyAgeComparator());

        personSet1.add( person1 );
        personSet1.add( person2 );
        personSet1.add( person3 );

        for (Person person : personSet1){
            System.out.println("Name: " + person.getName()+"    "+"Age: " + person.getAge());
        }
        System.out.println("\n"+"Person Sets: ");
        System.out.println(personSet);
        System.out.println(personSet1);
        System.out.println(" ");

        ///////////////////////////////

        //Creating hobby,address,country
        Hobby hobby1 = new Hobby( "Swimming", 4 );
        Hobby hobby2 = new Hobby( "Running", 3 );
        Hobby hobby3 = new Hobby( "Cycling", 7 );

        Address address1 = new Address( "Street no 1" );
        Address address2 = new Address( "Street no 2" );
        Address address3 = new Address( "Street no 3" );

        Country country1 = new Country("Romania");
        Country country2 = new Country("Germany");
        Country country3 = new Country("USA");

        // Assigning
        address1.addCountry( country1 );
        address1.addCountry( country2 );
        address1.addCountry( country3 );
        address2.addCountry( country1 );
        address3.addCountry( country2 );
        address3.addCountry( country3 );

        hobby1.addAddress( address1 );
        hobby1.addAddress( address3 );
        hobby2.addAddress( address2 );
        hobby3.addAddress( address3 );

        // Creating HashMap
        System.out.println("HashMap!!!");

        List<Hobby> hobbies = new ArrayList<>();
        hobbies.add( hobby1 );
        hobbies.add( hobby2 );
        hobbies.add( hobby3 );
        HashMap<Person,List<Hobby>> hashMap = new HashMap<Person,List<Hobby>>();
        hashMap.put( person1,hobbies );
        System.out.println("Person 1 hobby: "+"\n"+hashMap.get( person1 )+"\n");


        System.out.println("Another HashMap!!!"+"\n");

        HashMap<Person,Hobby> personHobbyListHashMap = new HashMap<Person, Hobby>();
        personHobbyListHashMap.put( person1, hobby1 );
        personHobbyListHashMap.put( person2, hobby2 );
        personHobbyListHashMap.put( person3, hobby3 );

        // for each
        for(Map.Entry<Person, Hobby> entry : personHobbyListHashMap.entrySet()) {
            Person key = entry.getKey();
            Hobby value = entry.getValue();
            System.out.println(entry.getKey()+" "+entry.getValue());
        }
        // personHobbyListHashMap.forEach((k,v) -> System.out.println("key: "+k+" value:"+v));

        System.out.println(" ");

        //print persons hobby
        System.out.println("Person 1 and 2 hobby:");
        System.out.println(personHobbyListHashMap.get( person1 ));
        System.out.println(personHobbyListHashMap.get( person2 ));

        System.out.println(" ");

        //print map keys

        hobby3.setHobbyName( "Walking" );
        System.out.println("Printing key set in map: ");
        System.out.println(personHobbyListHashMap.keySet());
        System.out.println(personHobbyListHashMap.entrySet());

//        hobby3.setFrequency( 5 );
        System.out.println("\n"+"Person 3 hobby:");
        System.out.println(personHobbyListHashMap.get( person3 ));

    }


}
