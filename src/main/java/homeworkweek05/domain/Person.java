package homeworkweek05.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Person {

    protected String name;
    protected int age;
    List<Hobby> hobbies = new ArrayList<>();

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

//    public void setName(String name) {
//        this.name = name;
//    }

    public int getAge() {
        return age;
    }

//    public void setAge(int age) {
//        this.age = age;
//    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Person person = (Person) o;
//        return Objects.equals( name, person.name ) && Objects.equals( age, person.age );
//    }


}