package homeworkweek05.domain;

public class Country {

    private String countryName;

    public Country(String countryName) {
        this.countryName = countryName;
    }

//    public String getCountryName() {
//        return countryName;
//    }

//    public void setCountryName(String countryName) {
//        this.countryName = countryName;
//    }

    @Override
    public String toString() {
        return "Country{" +
                "name='" + countryName + '\'' +
                '}';
    }
}
