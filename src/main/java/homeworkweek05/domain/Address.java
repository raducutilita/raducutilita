package homeworkweek05.domain;

import java.util.ArrayList;
import java.util.List;

public class Address {

    private String address;
    List<Country> countries = new ArrayList<>();

    public Address(String address) {
        this.address = address;
    }

//    public String getAddress() {
//        return address;
//    }

//    public void setAddress(String address) {
//        this.address = address;
//    }

    public void addCountry(Country country){
        this.countries.add( country );
    }

//    public void removeCountry(Country country){
//        this.countries.remove( country );
//    }

    @Override
    public String toString() {
        return "Address{" +
                "address='" + address + '\'' +
                ", countries=" + countries +
                '}';
    }
}

