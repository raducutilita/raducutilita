package homeworkweek05.domain;

import java.util.ArrayList;
import java.util.List;

public class Hobby{

    String hobbyName;

    int frequency;

    List<Address> addresses = new ArrayList<Address>();

    public Hobby(String hobbyName, int frequency) {
        this.hobbyName = hobbyName;
        this.frequency = frequency;
    }

    public void addAddress (Address address){
        this.addresses.add( address );
    }

//    public void removeAddress (Address address){
//        this.addresses.remove( address );
//    }

//    public String getHobbyName() {
//        return hobbyName;
//    }

    public void setHobbyName(String hobbyName) {
        this.hobbyName = hobbyName;
    }


//    public int getFrequency() {
//        return frequency;
//    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

//    public List<Address> getAddresses() {
//        return addresses;
//    }

//    public void setAddresses(List<Address> addresses) {
//        this.addresses = addresses;
//    }

    @Override
    public String toString() {
        return "Hobby{" +
                "hobbyName='" + hobbyName + '\'' +
                ", frequency=" + frequency +
                ", addresses=" + addresses +
                '}';
    }
}
