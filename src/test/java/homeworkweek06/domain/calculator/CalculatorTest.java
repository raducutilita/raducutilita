package homeworkweek06.domain.calculator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    private Calculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new Calculator();
    }

    @Test
    void fromKm_calculate_toMm() {

        double km_to_mm = calculator.km_to_mm( 1.2 );
        assertEquals( 1200000, km_to_mm );
    }

    @Test
    void fromM_calculate_toMm() {

        double m_to_mm = calculator.m_to_mm( 100 );
        assertEquals( 100000, m_to_mm );
    }

    @Test
    void fromDm_calculate_toMm() {

        double dm_to_mm = calculator.dm_to_mm( 13 );
        assertEquals( 1300, dm_to_mm );
    }

    @Test
    void fromCm_calculate_toMm() {

        double cm_to_mm = calculator.cm_to_mm( 30 );
        assertEquals( 300, cm_to_mm );
    }

    @Test
    void mm_addKm_result() {

        double result = calculator.mm( 30 )+ calculator.km_to_mm(0.001);
        calculator.expression();
        assertEquals( calculator.result(), result );
    }

    @Test
    void inputSINumber1_addSINumber2_resultInMm() {

        double number1InKm = calculator.km_to_mm( 1 );
        double number2InM = calculator.m_to_mm( 1000 );
        assertEquals( calculator.result(), number1InKm + number2InM );
    }


}