package homeworkweek06.domain.calculator;


import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;
import org.junit.platform.suite.api.SuiteDisplayName;

@Suite
@SuiteDisplayName( "Calculator Tests" )
@SelectClasses( {CalculatorTest.class, CalculatorParameterizedTest.class} )
public class TestSuite {
}
