package homeworkweek06.domain.calculator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorParameterizedTest {

    private Calculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new Calculator();
    }

    @ParameterizedTest
    @ValueSource(doubles = {2.5,0.005,0.01345})
    void fromKm_calculate_toMm(double kilometers) {

        double testKm = calculator.km_to_mm(kilometers);
        assertEquals( calculator.result(), testKm );
    }
}
